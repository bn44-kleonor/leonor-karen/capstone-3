//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const scheduleSchema = new Schema({
	month:{
		type: String
	},
	date:{
		type: String
	},
	isAvailable:{
		type: Boolean,
		default: true
	}
});

//export schema as model
module.exports = mongoose.model("Schedule", scheduleSchema);
