//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const serviceSchema = new Schema({
	title:{
		type: String,
		required: true
	},
	url:{
		type: String
	},
	comments:{
		type: String
	},
	date:{
		type: String
	},
	imageLocation: {
		type: String
	}
});

//export schema as model
module.exports = mongoose.model("Service", serviceSchema);
