//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const bookingSchema = new Schema({
	isApproved:{
		type: Boolean,
		default: false
	},
	isActive:{
		type: Boolean,
		default: true
	},
	userId:{
		type: String
	},
	scheduleId:{
		type: String
	},
	serviceId:{
		type: String
	}
})

//export schema as model
module.exports = mongoose.model("Booking", bookingSchema);
