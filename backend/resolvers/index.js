//MODELS
const User = require('../models/user');
const Service = require('../models/service');
const Schedule = require('../models/schedule');
const Log = require("../models/log");
const Booking = require('../models/booking');

//dependency
const bcrypt = require("bcrypt");
const auth = require("../jwt-auth");
const validToken = require("../jwt-verify");
const uuid = require("uuid/v1");
const fs = require("fs");

const resolvers = {
	User: {
		logs: ({ _id }, args)=>{
			return Log.find({userId: _id})
		}, 
		bookings: ({ _id }, args) => {
			return Booking.find({ userId: _id });
		}
	},
	Log: {
		user: ({ userId}, args) => {
			return User.findById(userId)
		}
	},
	Service: {
		bookings: ({ _id }, args) => {
			return Booking.find({ serviceId: _id });
		}
	},
	Schedule: {
		bookings: ({ _id }, args) => {
			return Booking.find({ scheduleId: _id });
		}
	},
	Booking: {
		user: ({ userId }, args) => {
			return User.findById(userId);
		},
		service: ({ serviceId }, args) => {
			return Service.findById(serviceId);
		},
		schedule: ({ scheduleId }, args ) => {
			return Schedule.findById(scheduleId);
		}
	},
	Query: {
		users: () => {
			return User.find({});
		},
		logs: ()=> {
			return Log.find({})
		},
		services: () => {
			return Service.find({});
		},
		schedules: () => {
			return Schedule.find({});
		},
		bookings: () => {
			return Booking.find({});
		},
		user: (parent, { id }) => {
			return User.findById(id);
		},
		log: (parent, {id}) =>{
			return Log.findById(id)
		},
		booking: (parent, { id }) => {
			return Booking.findById(id);
		},
		service: (parent, { id }) => {
			return Service.findById(id);
		},
		schedule: (parent, { id }) => {
			return Schedule.findById(id);
		}
	},

	Mutation: {
		registerUser: (parent, {firstName, lastName, email, password}) => {
			//hash
			let user = new User({
				firstName,
				lastName,
				email,
				password: bcrypt.hashSync(password, 8)
				// password
			});

			return user.save().then((user, err) => {
				return err ? false : true;
			});
		},
		loginUser: (parent, { email, password }) => {
			//validation
			let query = User.findOne({ email });
			return query.then(user => {
				if (user === null){
					return null;
				}

				//unhash password
				let isPasswordMatched = bcrypt.compareSync(password, user.password);

				//create login token
				if (isPasswordMatched) {
					user.token = auth.createToken(user.toObject());
					return user;
				} else {
					return null;
				}
			});
		},
		storeService: (parent, { title, url, comments, date, base64EncodedImage }) => {
			//validation
			//verify and decode token
			// console.log(base64EncodedImage);

			let base64Image = base64EncodedImage.split(";base64,").pop();
			let imageLocation = "images/" + uuid() + ".png";
			fs.writeFile(imageLocation, base64Image, {encoding:"base64"}, err => {
				console.log(err);
			});

			let service = new Service({
				title,
				url,
				comments,
				date,
				imageLocation: imageLocation
			});
			return service.save().then((asset, err)=>{
				return err ? console.log(err) : asset;
			});
		},
		storeSchedule: (parent, { month, date, isAvailable, bookings}) => {
			//validation
			//verify and decode token
			let schedule = new Schedule({
				month,
				date,
				// isAvailable,
				bookings
			});
			return schedule.save();
		},
		storeBooking: (parent, { isApproved, isActive, userId, serviceId, scheduleId}) => {
			//verify and decode token
			//check if userId, serviceId, scheduleId exists
			let booking = new Booking({
				isApproved,
				isActive,
				userId,
				serviceId,
				scheduleId
			});
			return booking.save();
		},

		updateUser:(parent, {id, firstName, lastName, email, password}) => {
			return User.findByIdAndUpdate(id, {firstName, lastName, email, password})
		},

		updateService:(parent, {id, title, url, comments, date}) => {
			return Service.findByIdAndUpdate(id, {title, url, comments, date})
		},

		updateSchedule: (parent, { id, month, date, isAvailable}) => {
			return Schedule.findByIdAndUpdate(id, {month, date, isAvailable})
		},

		updateBooking: (parent, { id, isApproved, isActive, userId, serviceId, scheduleId}) => {
			return Booking.findByIdAndUpdate(id, {isApproved, isActive, userId, serviceId, scheduleId})
		},

		destroyUser: (parent, {id}) => {
			return User.findByIdAndRemove(id)
		},

		destroyService: (parent, {id}) => {
			return Service.findByIdAndRemove(id)
		},

		destroySchedule: (parent, {id}) => {
			return Schedule.findByIdAndRemove(id)
		},

		destroyBooking: (parent, {id}) => {
			return Booking.findByIdAndRemove(id)
		}
	}
};

module.exports = resolvers;
