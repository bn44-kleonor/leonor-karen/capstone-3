//1) declare dependencies
const express = require("express");
const app = express();
const cors = require('cors');
const { ApolloServer } = require("apollo-server-express");
const typeDefs = require("./typeDefs");
const resolvers = require("./resolvers");
// const stripe = require('stripe')('mongodb+srv://admin:<admin1234>@cluster0-rifyw.mongodb.net/test?retryWrites=true&w=majority');

const databaseUrl = process.env.DATABASE_URL || "mongodb+srv://admin:admin1234@cluster0-rifyw.mongodb.net/amapp?retryWrites=true&w=majority"


//3) connect to db
const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/booking', {
// mongoose.connect(databaseUrl, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useCreateIndex: true
});
mongoose.connection.once('open',()=>{
	console.log("Now connected to local MongoDB server.");
});

//apollo server
const server = new ApolloServer({
	typeDefs,
	resolvers,
	playground: true,
	introspection: true
})

server.applyMiddleware({
	app,
	path: "/graphql"
})

//connect the frontend and backend
app.use(cors());

app.use(express.json({ limit: '50mb' }));
// app.use('/images', express.static('images'));

const porta = 4001;
const PORT = process.env.PORT || porta;

//2) initialize server

// console.log(`${port}`);
app.listen(PORT, ()=>{
	console.log(`Now listening for requests on port ${PORT}`);
});

//http://localhost:4001/graphql
//http://localhost:4000/user/id