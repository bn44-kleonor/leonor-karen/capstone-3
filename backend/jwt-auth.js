const jwt = require("jsonwebtoken");
const secret = "test_booking";

module.exports.createToken = (user) => {
    let data = {
        _id: user.id,
        email: user.email,
        isAdmin: user.isAdmin,
        firstName: user.firstName,
        lastName: user.lastName
    }

    return jwt.sign(data, secret, { expiresIn: '2h'})
}