import React, {useState} from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import jwt from 'jsonwebtoken';

//pages
import AppNavbar from './partials/AppNavbar';
import NotFoundPage from './pages/NotFoundPage';
import RegisterPage from './pages/RegisterPage';
import LoginPage from './pages/LoginPage';
import UserPage from './pages/UserPage';
import ServicePage from './pages/ServicePage';
import ServiceShow from './components/show/ServiceShow';
import SchedulePage from './pages/SchedulePage';
import ScheduleShow from './components/show/ScheduleShow';
import DashboardPage from './pages/DashboardPage';

const App = () => {
console.log(localStorage.getItem("token"));

    return (
        <BrowserRouter>
            <AppNavbar/>
            <Switch>
                <Route exact path="/" />
                <Route component={UserPage} exact path="/users" />
                <Route component={ServicePage} path="/services"/>
                <Route component={ServiceShow} exact path ="/service/:id"/>
                <Route component={RegisterPage} exact path ="/register"/>
                <Route component={LoginPage} exact path ="/login"/>
                <Route component={SchedulePage} path="/schedules"/>
                <Route component={ScheduleShow} exact path ="/schedules/:id"/>
                <Route component={DashboardPage} exact path="/dashboard"/>
                <Route compoent={NotFoundPage}/>
            </Switch>
        </BrowserRouter>
    )
}

export default App;