import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import './index.css';

import App from './app';

// apollo client
const client = new ApolloClient({ uri: 'http://localhost:4001/graphql'});
// const client = new ApolloClient({uri: "https://bwtestbook.herokuapp.com/graphql"});

const root = document.querySelector("#root");
const pageComponent = (
		// <Navbar className='is-black'>
	    //   <Navbar.Brand>
	    //     <Navbar.Item>
	    //     	<strong>Test Booking</strong>
	    //     </Navbar.Item>
	    //   </Navbar.Brand>
	    // </Navbar>
	<ApolloProvider  client={client}>
		<App/>
	</ApolloProvider>
);

ReactDOM.render(pageComponent, root);
