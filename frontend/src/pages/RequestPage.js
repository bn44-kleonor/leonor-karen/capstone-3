import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Section, Heading, Columns } from 'react-bulma-components';

const RequestPage = props => {
	return (
		<Section size='medium'>
	      <Heading>Requests</Heading>
	      <Columns>
	        <Columns.Column>
	          <p>Form</p>
	        </Columns.Column>
	        <Columns.Column>
	          <p>Table</p>
	        </Columns.Column>
	      </Columns>
	    </Section>
	)
}

export default RequestPage;