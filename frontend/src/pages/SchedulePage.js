import React, { useState } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { 
	Section,
	Heading,
	Columns
} from 'react-bulma-components';
import ScheduleAdd from '../components/forms/ScheduleAdd';
import ScheduleList from '../components/lists/ScheduleList';

import compose from 'lodash.flowright';
import { graphql } from 'react-apollo';

import { getSchedulesQuery, getScheduleQuery } from '../graphql/queries';
import {
	storeScheduleMutation,
	destroyScheduleMutation,
	updateScheduleMutation
} from '../graphql/mutations';

import Swal from 'sweetalert2';

const SchedulePage = (props) => {
	//console.log(props.data.schedules);

	// ADD Schedule
	const [schedules, setSchedules] = useState([]);
	const data = props.getSchedulesQuery;
	
	const addSchedule = (newSchedule) => {
		props.storeScheduleMutation({
			variables: newSchedule,
			refetchQueries: [{ query: getSchedulesQuery}]
		})
      .then(response => {
		console.log("response");
        console.log(response);
        let name = response.data.addSchedule.name;
        if (name) {
          Swal.fire({
            title: 'Added A Schedule',
            text: `${name} has been added!`,
            icon: 'success',
            showConfirmButton: false,
            timer: 3000
          });
        }
      })
    //   .catch(err => {
    //     console.log(err);
    //     Swal.fire({
    //       title: 'Image Is Too Large',
    //       text: 'File limit is 20mb.',
    //       icon: 'error'
    //     });
    //   });
	};

	// DELETE Schedule
	const destroySchedule = (id, title) => {
		Swal.fire({
			title: "Delete Schedule?",
			text: `Do you want to remove ${title} ?`,
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Delete",
			confirmButtonColor: "#ff3860",
			reverseButtons: true
		}).then((formData)=>{
			if(formData.value){
				console.log("idedelete daw");
				props.destroyScheduleMutation({
					variables: { id: id },
					refetchQueries: [{ query: getSchedulesQuery }]
				})
				.then(response => {
					console.log(response);
					let result = response.data.destroySchedule;

					if(result){
						Swal.fire({
							title: "Schedule Deleted",
							text: `${result.title} has been deleted.`,
							type: "success"
						})
					}
				})
			}
		})
	}

	// UPDATE Schedule
	const updateSchedule = (id, month, date, isAvailable) => {
		Swal.fire({
			title: "Update Schedule",
			type: "info",
			html:
				`<input id="month" class="swal2-input" value="${month}">` + 
				`<input id="date" class="swal2-input" value="${date}">` +
				`<input id="isAvailable" class="swal2-input" value="${isAvailable}">`,
			preConfirm: () => {
				let newMonth = document.querySelector("#month").value;
				let newDate = document.querySelector("#date").value;
				let newIsAvailable = document.querySelector("#isAvailable").value;

				if(newMonth === "" || newDate === "" || newIsAvailable === ""){
					Swal.showValidationMessage("Please fill out all fields");
				} else {
					return {
						id: id,
						month: newMonth,
						date: newDate,
						isAvailable: newIsAvailable
					}
				}
			}
		})
		.then(formData => {
			if(formData.value) {
				props.updateScheduleMutation({
					variables: formData.value,
					refrechQueries: [{ query: getSchedulesQuery }]
				})
				.then(response => {
					let result = response.data.updateSchedule;
					if(result) {
						Swal.fire({
							title: `Schedule Updated`,
							text: `The schedule has been updated to date "${date}"`,
							type: "success",
							showConfirmButton: false,
							timer: 3000
						})
					}
				})
			}
		})
	}

	// VIEW Schedule
	const viewSchedule = (id, month, date, isAvailable) => {
		Swal.fire({
			title: "View Schedule",
			type: "info",
			html:
				`<input id="month" class="swal2-input" value="${month}" disabled>` + 
				`<input id="date" class="swal2-input" value="${date}" disabled>` +
				`<input id="isAvailable" class="swal2-input" value="${isAvailable}" disabled>`,
		})
	}


	//Display TABLE
	const sectionStyle = {
		paddingTop: "3em",
		paddingBottom: "3em"
	}
	return(
		<Section size='medium' style={ sectionStyle }>
			<Heading>Schedules</Heading>
			<Columns>
				<Columns.Column size={4}>
					<ScheduleAdd addSchedule={addSchedule}/>
				</Columns.Column>
				<Columns.Column>
					<ScheduleList
						schedules={data.schedules}
						destroySchedule={destroySchedule}
						updateSchedule={updateSchedule}
						viewSchedule={viewSchedule}
					/>
				</Columns.Column>
			</Columns>
		</Section>
	)
}

// export default graphql(getSchedulesQuery)(SchedulePage);
export default compose(
	graphql(getSchedulesQuery, { name: 'getSchedulesQuery'}),
	graphql(storeScheduleMutation, { name: 'storeScheduleMutation'}),
	graphql(destroyScheduleMutation, { name: 'destroyScheduleMutation'}),
	graphql(updateScheduleMutation, { name: 'updateScheduleMutation'}),
	graphql(getScheduleQuery, { name: 'getScheduleQuery'})
)(SchedulePage);