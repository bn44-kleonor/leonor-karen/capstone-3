//react built-in and template components
import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Section, Heading, Columns} from 'react-bulma-components';
import {graphql} from 'react-apollo';

//frontend components
import UserAdd from '../components/forms/UserAdd';
import UserList from '../components/lists/UserList';

//backend connect
import {getUsersQuery} from '../graphql/queries';

const UserPage = props => {
    // console.log(props.data.users);

    const data = props.data;

    return (
        <Section size='medium'>
            <Heading>Users</Heading>
            <Columns>
                {/*<Columns.Column>*/}
                    {/*<UserAdd/>*/}
                {/*</Columns.Column>*/}
                <Columns.Column>
                    <UserList users={data.users}/>
                </Columns.Column>
            </Columns>
        </Section>
    )
}

export default graphql(getUsersQuery)(UserPage);