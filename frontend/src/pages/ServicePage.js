import React, { useState } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { 
	Section,
	Heading,
	Columns
} from 'react-bulma-components';
import ServiceAdd from '../components/forms/ServiceAdd';
import ServiceList from '../components/lists/ServiceList';

import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

import compose from 'lodash.flowright';
import { graphql } from 'react-apollo';

import { getServicesQuery, getServiceQuery } from '../graphql/queries';
import {
	storeServiceMutation,
	destroyServiceMutation,
	updateServiceMutation
} from '../graphql/mutations';

import Swal from 'sweetalert2';

const ServicePage = (props) => {
	//console.log(props.data.services);

	// ADD Service
	const [services, setServices] = useState([]);
	const data = props.getServicesQuery;
	
	const addService = (newService) => {
		props.storeServiceMutation({
			variables: newService,
			refetchQueries: [{ query: getServicesQuery}]
		})
      .then(response => {
        console.log(response);
        // let name = response.data.addService.title;
        let name = response.data.storeService.title;
        if (name) {
          Swal.fire({
            title: 'Booking Successful!',
            text: `${name} has been added!`,
            icon: 'success',
            showConfirmButton: false,
            timer: 3000
          });
        }
      })
      .catch(err => {
        console.log(err);
        Swal.fire({
          title: 'Image Is Too Large',
          text: 'File limit is 20mb.',
          icon: 'error'
        });
      });
	};

	// DELETE Service
	const destroyService = (id, title) => {
		Swal.fire({
			title: "Delete Service?",
			text: `Do you want to remove ${title} ?`,
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Delete",
			confirmButtonColor: "#ff3860",
			reverseButtons: true
		}).then((formData)=>{
			if(formData.value){
				console.log("idedelete daw");
				console.log(formData);
				props.destroyServiceMutation({
					variables: { id: id },
					refetchQueries: [{ query: getServicesQuery }]
				})
				.then(response => {
					console.log("mag Swal daw na idedelete");
					console.log(response);
					console.log(title);
					let result = response.data.destroyService;

					if(formData){
						Swal.fire({
							title: "Booking Deleted",
							text: `${title} has been deleted.`,
							type: "success"
						})
					}
				})
			}
		})
	}

	// UPDATE Service
	const updateService = (id, title, url, comments, date) => {
		Swal.fire({
			title: "Update Service",
			type: "info",
			html:
				`<input id="title" class="swal2-input" value="${title}">` + 
				`<input id="url" class="swal2-input" value="${url}">` +
				`<input id="comments" class="swal2-input" value="${comments}">` +
				`<input id="date" class="datepicker" value="${date}">`,
				// `<DatePicker id="date" class="datepicker" value={date}/>`,
				// ' <input data-toggle="datepicker" type="text" id="date" class="swal2-input">',
				// confirmButtonText: 'Next &rarr;',
				// showCancelButton: true,
				// onOpen: function() {
				// 	('[data-toggle="Datepicker"]').Datepicker({
				// 		startView: 2,
				// 		autoHide: true,
				// 		inline: true,
				// 		zIndex: 999999
				// 	});
				// },
			preConfirm: () => {
				let newTitle = document.querySelector("#title").value;
				let newUrl = document.querySelector("#url").value;
				let newComments = document.querySelector("#comments").value;
				let newDate = document.querySelector("#date").value;

				if(newTitle === "" || newUrl === "" || newComments === "" || newDate === ""){
					Swal.showValidationMessage("Please fill out all fields");
				} else {
					return {
						id: id,
						title: newTitle,
						url: newUrl,
						comments: newComments,
						date: newDate
					}
				}
			}
		})
		.then(formData => {
			if(formData.value) {
				props.updateServiceMutation({
					variables: formData.value,
					refrechQueries: [{ query: getServicesQuery }]
				})
				.then(response => {
					let result = response.data.updateService;
					if(result) {
						Swal.fire({
							title: `Service Updated`,
							text: `The service "${title}" has been updated`,
							type: "success",
							showConfirmButton: false,
							timer: 3000
						})
					}
				})
			}
		})
	}

	// VIEW Service
	const viewService = (id, title, url, comments, date) => {
		Swal.fire({
			title: "View Service",
			type: "info",
			html:
				`<input id="title" class="swal2-input" value="${title}" disabled>` + 
				`<input id="url" class="swal2-input" value="${url}" disabled>` +
				`<input id="comments" class="swal2-input" value="${comments}" disabled>` +
				`<input id="date" class="swal2-input" value="${date}" disabled>`,
		})
	}


	//Display TABLE
	const sectionStyle = {
		paddingTop: "3em",
		paddingBottom: "3em"
	}
	return(
		<Section size='medium' style={ sectionStyle }>
			<Heading>Services</Heading>
			<Columns>
				<Columns.Column size={4}>
					<ServiceAdd addService={addService}/>
				</Columns.Column>
				<Columns.Column>
					<ServiceList
						services={data.services}
						destroyService={destroyService}
						updateService={updateService}
						viewService={viewService}
					/>
				</Columns.Column>
			</Columns>
		</Section>
	)
}

// export default graphql(getServicesQuery)(ServicePage);
export default compose(
	graphql(getServicesQuery, { name: 'getServicesQuery'}),
	graphql(storeServiceMutation, { name: 'storeServiceMutation'}),
	graphql(destroyServiceMutation, { name: 'destroyServiceMutation'}),
	graphql(updateServiceMutation, { name: 'updateServiceMutation'}),
	graphql(getServiceQuery, { name: 'getServiceQuery'})
)(ServicePage);