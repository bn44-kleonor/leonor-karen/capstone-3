import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Section, Heading, Columns } from 'react-bulma-components';

const NotFoundPage = props => {
	return (
		<Section size='medium'>
			<Heading>Page Not Found</Heading>
		</Section>
	)
}

export default NotFoundPage;
