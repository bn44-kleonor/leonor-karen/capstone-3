import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Section, Heading, Columns } from 'react-bulma-components';

import LoginForm from '../components/forms/LoginForm';

const LoginPage = props => {
	console.log(props);
	return (
		<Section size='medium' className='sectionStyle'>
			<Heading>Login</Heading>
			<Columns>
				<Columns.Column className='is-4'>
					<LoginForm/>
				</Columns.Column>
			</Columns>
		</Section>
	);
};

export default LoginPage;
