import {gql} from 'apollo-boost';

// USER
const getUsersQuery = gql `
    {
        users{
            id
            firstName
            lastName
            email
            isAdmin
            bookings {
                id
                isApproved
                isActive
                userId
                serviceId
                scheduleId
            }
        }
    }
`;

const getUserQuery = gql`
    query ($id: ID!) {
        user(id: $id){
            id
            firstName
            lastName
            email
            isAdmin
            bookings {
                id
                isApproved
                isActive
                userId
                serviceId
                scheduleId
            }
        }
    }
`;

// SERVICE
const getServicesQuery = gql`
    {
        services {
            id
            title
            url
            comments
            date
            bookings {
                id
                isApproved
                isActive
                userId
                serviceId
                scheduleId
            }
        }
    }
`;

const getServiceQuery = gql`
    query ($id: ID!) {
        service(id: $id){
            id
            title
            url
            comments
            date
            imageLocation
            bookings {
                id
                isApproved
                isActive
                userId
                serviceId
                scheduleId
            }
        }
    }    

`;

// SCHEDULE
const getSchedulesQuery = gql`
    {
        schedules {
            id
            month
            date
            isAvailable
            bookings {
                id
                isApproved
                isActive
                userId
                serviceId
                scheduleId
            }
        }
    }
`;

const getScheduleQuery = gql`
    query($id: ID!){
        schedule(id: $id){
            id
            month
            date
            isAvailable
            bookings {
                id
                isApproved
                isActive
                userId
                serviceId
                scheduleId
            }
        }
    }
`;




export { 
    getUsersQuery, 
    getUserQuery, 
    getServicesQuery, 
    getServiceQuery,
    getSchedulesQuery, 
    getScheduleQuery 
};