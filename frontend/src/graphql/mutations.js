import { gql } from 'apollo-boost';

const registerUserMutation = gql `
    mutation(
        $firstName: String
        $lastName: String
        $email: String!
        $password: String!
    ){
        registerUser(
            firstName: $firstName
            lastName: $lastName
            email: $email
            password: $password
        )
    }
`;

const loginMutation = gql`
    mutation(
        $email: String!
        $password: String!
    ){
        loginUser(
            email: $email
            password: $password 
        ) {
            firstName
            lastName
            isAdmin
            token
        }
    }
`;

// SERVICE
const storeServiceMutation = gql `
    mutation(
        $title: String!
        $url: String!
        $comments: String
        $date: String
        $base64EncodedImage: String
    ) {
        storeService(
            title: $title
            url: $url
            comments: $comments
            date: $date
            base64EncodedImage: $base64EncodedImage
        ) {
            title
            url
        }
    }
`;

const destroyServiceMutation = gql `
    mutation($id: ID!) {
        destroyService(id: $id) {
            title
        }
    }
`;

const updateServiceMutation = gql`
    mutation(
        $id: ID!
        $title: String!
        $url: String
        $comments: String
        $date: String
    ) {
        updateService(
            id: $id
            title: $title
            url: $url
            comments: $comments
            date: $date
        ) {
            title
            url
            comments
            date
        }
    }
`;

// SCHEDULE

const storeScheduleMutation = gql `
    mutation(
        $month: String
        $date: String!
    ) {
        storeSchedule(
            month: $month
            date: $date
        ) {
            month
            date
        }
    }
`;

const destroyScheduleMutation = gql `
    mutation($id: ID!){
        destroySchedule(id: $id) {
            date
        }
    }
`;

const updateScheduleMutation = gql`
    mutation(
        $id: ID!
        $month: String
        $date: String
    ) {
        updateSchedule(
            id: $id
            month: $month
            date: $date
        ) {
            month
            date
        }
    }
`;


export { 
    registerUserMutation, 
    storeServiceMutation, 
    destroyServiceMutation, 
    updateServiceMutation,
    storeScheduleMutation,
    destroyScheduleMutation,
    updateScheduleMutation,
    loginMutation 
};
