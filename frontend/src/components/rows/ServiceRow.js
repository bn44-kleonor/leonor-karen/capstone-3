import React from 'react';
import {Link} from 'react-router-dom';

const ServiceRow = (props) => {
	//console.log(props)
	const service = props.service;
	// console.log("service below");
	// console.log(service);

	return(
		<tr>
			<td>{service.title}</td>
			<td>{service.url}</td>
			<td>{service.comments}</td>
			<td>{service.date}</td>
			<td>
				<button 
					className='button is-danger'
					onClick={() => props.destroyService(
							service.id, 
							service.title
						)}
				>
					Remove
				</button>
				&nbsp;
				<button 
					className='button is-warning'
					onClick={()=> props.updateService(
							service.id,
							service.title,
							service.url,
							service.comments,
							service.date
						)}
				>
					Update
				</button>
				&nbsp;
				<Link to={`/service/${service.id}`}>
					<button className='button is-primary'>View</button>
				</Link>
			</td>
		</tr>
	)
}

export default ServiceRow;

// <Link to={`/service/${service.id}`}>
// 	<button className='button is-primary'>View</button>
// </Link>

// <button 
// 	className='button is-warning'
// 	onClick={()=> props.viewService(
// 			service.id,
// 			service.title,
// 			service.url,
// 			service.comments,
// 			service.date
// 		)}
// >
// 	View
// </button>