import React from 'react';
import { Link } from 'react-router-dom';

const UserRow = props => {
  // console.log(props)
  const user = props.user;
	return(
		<tr>
            <td>{user.firstName}</td>
            <td>{user.lastName}</td>
            <td>{user.email}</td>
            <td>{user.isAdmin}</td>
            <td>
              <button className='button is-danger'>Remove</button>
              &nbsp;
              <button className='button is-warning'>Update</button>
              &nbsp;
              <Link to={`/user/${user.id}`}>
                <button className='button is-primary'>View</button>
              </Link>
            </td>
          </tr>
	)
}

export default UserRow;