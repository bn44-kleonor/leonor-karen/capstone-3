import React from 'react';
import {Link} from 'react-router-dom';

const ScheduleRow = (props) => {
	//console.log(props)
	const schedule = props.schedule;
	let booked = "abc";
	// console.log("schedule below");
	// console.log(schedule);
	if(schedule.isAvailable == true)
	{
		console.log(schedule.isAvailable);
		booked = "Booked";
	} else {
		booked = "Available";
		console.log(schedule.isAvailable);
	}

	return(
		<tr>
			<td>{schedule.month}</td>
			<td>{schedule.date}</td>
			<td>{booked}</td>
			<td>
				<button 
					className='button is-danger'
					onClick={() => props.destroySchedule(
							schedule.id, 
							schedule.date
						)}
				>
					Remove
				</button>
				&nbsp;
				<button 
					className='button is-warning'
					onClick={()=> props.updateSchedule(
							schedule.id,
							schedule.month,
							schedule.date,
							schedule.isAvailable
						)}
				>
					Update
				</button>
				&nbsp;
				<Link to={`/schedule/${schedule.id}`}>
					<button className='button is-primary'>View</button>
				</Link>
			</td>
		</tr>
	)
}

export default ScheduleRow;

// <Link to={`/schedule/${schedule.id}`}>
// 	<button className='button is-primary'>View</button>
// </Link>

// <button 
// 	className='button is-warning'
// 	onClick={()=> props.viewSchedule(
// 			schedule.id,
// 			schedule.title,
// 			schedule.url,
// 			schedule.comments
// 		)}
// >
// 	View
// </button>