import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';

import { graphql } from 'react-apollo';
import compose from 'lodash.flowright';
import {registerUserMutation} from '../../graphql/mutations';

const RegisterForm = props => {
  const [ firstName, setFirstName] = useState("");
  const [ lastName, setLastName ] = useState("");
  const [ email, setEmail ] = useState("");
  const [ password, setPassword ] = useState("");
  const [ isDisabled, setIsDisabled ] = useState(true);
  const [ goToLogin, setGoToLogin ] = useState(false);

  const checkPassword = (password) => {
    setPassword(password);
    if(password.length >= 8 ){
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }

  // register
  const register = e => {
    e.preventDefault();
    props.registerUserMutation({
      variables: {
        firstName, lastName, email, password
      }
    })
    .then(response => {
      const newUser = response.data.registerUser;

      if(newUser){
        Swal.fire({
          title: "Register Successful",
          text: "You will be redirected to login page.",
          icon: "success"
        })
        .then(()=>{
          setGoToLogin(true);
        })
      } else {
        Swal.fire({
          title: "Registration Failed",
          text: "The server encountered an error",
          icon: "error"
        })
      }
    })
  }

  //redirect to login once registered
  if(goToLogin){
    return <Redirect to="/login?register=true"/>
  }
        return (
                <form onSubmit={e => register(e)}>
                  <div className='field'>
                    <label className='label has-text-weight-normal'>First Name</label>
                    <div className='control'>
                      <input 
                        className='input' 
                        type='text'
                        value={firstName}
                        onChange={e => setFirstName(e.target.value)}
                      />
                    </div>
                  </div>

                  <div className='field'>
                    <label className='label has-text-weight-normal'>Last Name</label>
                    <div className='control'>
                      <input 
                        className='input' 
                        type='text'
                        value={lastName}
                        onChange={e => setLastName(e.target.value)}
                      />
                    </div>
                  </div>

                  <div className='field'>
                    <label className='label has-text-weight-normal'>Email</label>
                    <div className='control'>
                      <input 
                        className='input' 
                        type='email'
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                      />
                    </div>
                  </div>


                  <div className='field'>
                    <label className='label has-text-weight-normal'>Password</label>
                    <div className='control'>
                      <input 
                        className='input' 
                        type='password'
                        value={password}
                        onChange={e => checkPassword(e.target.value)}
                      />
                    </div>
                  </div>

                  <div className='field'>
                    <div className='control'>
                      <button 
                        type='submit' 
                        className='button is-link is-primary'
                        disabled={ isDisabled }
                      >
                        Register
                      </button>
                      &nbsp;
                    </div>
                  </div>

            </form>
        );
}
 
export default compose(
  graphql(registerUserMutation, {name: 'registerUserMutation'})
  )(RegisterForm);
