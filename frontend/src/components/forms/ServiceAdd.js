import React, { useState } from 'react';
import Swal from 'sweetalert2';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Card } from 'react-bulma-components';

// date picker
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import DayPickerInput from 'react-day-picker/DayPickerInput';

import { graphql } from 'react-apollo';
import { getServiceQuery } from '../../graphql/queries';
import { toBase64 } from '../../functions';

const ServiceAdd = props => {
  //console.log("ServiceAdd props")
  //console.log(props);

  const now = new Date();
  
  const [title, setTitle] = useState("");
  const [url, setUrl] = useState("");
  const [comments, setComments] = useState("");
  const [date, setDate] = useState(now.toISOString().substring(0,10));
  const fileRef = React.createRef();

  const addService = (e) => {
    e.preventDefault();

    toBase64(fileRef.current.files[0]).then(encodedFile => {
      // console.log(encodeFile);

      const newService = {
        title: title,
        url: url,
        comments: comments,
        date: date,
        base64EncodedImage: encodedFile
      };

      setTitle('');
      setUrl('');
      setComments('');
      setDate('');

      props.addService(newService);

    });

    Swal.fire({
      title: "Service Added",
      text: `${title} has been added!`,
      type: "success"
    })
  }

	return (
		<Card>
            <Card.Header>
              <Card.Header.Title>Add Service</Card.Header.Title>
            </Card.Header>
            <Card.Content>
              <form onSubmit={addService}>

                <div className='field'>
                  <label className='label has-text-weight-normal'>
                    Image
                  </label>
                  <div className='control'>
                    <input 
                      className='input' 
                      type='file' 
                      required 
                      ref={fileRef}
                      accept="image/png"
                    />
                  </div>
                </div>

                <div className='field'>
                  <label className='label has-text-weight-normal'>
                    Title
                  </label>
                  <div className='control'>
                    <input 
                      className='input' 
                      type='text' 
                      required
                      value={title}
                      onChange={e =>
                        {
                          setTitle(e.target.value);
                        }
                      }
                    />
                  </div>
                </div>


                <div className='field'>
                  <label className='label has-text-weight-normal'>
                    URL
                  </label>
                  <div className='control'>
                    <input 
                      className='input' 
                      type='text' 
                      required 
                      value={url}
                      onChange={e =>
                        {
                          setUrl(e.target.value);
                        }
                      }
                    />
                  </div>
                </div>

                <div className='field'>
                  <label className='label has-text-weight-normal'>
                    Comments
                  </label>
                  <div className='control'>
                    <input 
                      className='input' 
                      type='text' 
                      required 
                      value={comments}
                      onChange={e => 
                        {
                          setComments(e.target.value);
                        }
                      }
                    />
                  </div>
                </div>

                <div className='field'>
                  <label className='label has-text-weight-normal'>
                    Date
                  </label>
                  <div className='control'>
                      <DatePicker 
                      value={date} 
                      onChange={e => {
                        // console.log(e);
                        setDate(e.toISOString().substring(0,10));
                      }}
                      />
                  </div>
                </div>

                <br />

                <div className='field'>
                  <div className='control'>
                    <button type='submit' className='button is-link is-success'>
                      Add
                    </button>
                  </div>
                </div>
              </form>
            </Card.Content>
          </Card>
	)
}

export default  graphql(getServiceQuery)(ServiceAdd);