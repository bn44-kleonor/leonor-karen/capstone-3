import React, { useState } from 'react';
import Swal from 'sweetalert2';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Card } from 'react-bulma-components';

import { graphql } from 'react-apollo';
import { getScheduleQuery } from '../../graphql/queries';
import { toBase64 } from '../../functions';

const ScheduleAdd = props => {
  //console.log("ScheduleAdd props")
  //console.log(props);

  const [month, setMonth] = useState("");
  const [date, setDate] = useState("");
  const [isAvailable, setIsAvailable] = useState("");
  const fileRef = React.createRef();

  const addSchedule = (e) => {
    e.preventDefault();

    // toBase64(fileRef.current.files[0]).then(encodedFile => {
    // console.log(encodeFile);

      const newSchedule = {
        month: month,
        date: date,
        isAvailable: isAvailable,
        // base64EncodedImage: encodedFile
      };

      setMonth('');
      setDate('');
      setIsAvailable('');

      props.addSchedule(newSchedule);

    // });

    Swal.fire({
      title: "Schedule Added",
      text: `${date} has been added!`,
      type: "success"
    })
  }

  return (
    <Card>
            <Card.Header>
              <Card.Header.Title>Add Schedule</Card.Header.Title>
            </Card.Header>
            <Card.Content>
              <form onSubmit={addSchedule}>

                <div className='field'>
                  <label className='label has-text-weight-normal'>
                    Month
                  </label>
                  <div className='control'>
                    <input 
                      className='input' 
                      type='text' 
                      required
                      value={month}
                      onChange={e =>
                        {
                          setMonth(e.target.value);
                        }
                      }
                    />
                  </div>
                </div>


                <div className='field'>
                  <label className='label has-text-weight-normal'>
                    Date
                  </label>
                  <div className='control'>
                    <input 
                      className='input' 
                      type='text' 
                      required 
                      value={date}
                      onChange={e =>
                        {
                          setDate(e.target.value);
                        }
                      }
                    />
                  </div>
                </div>

                <br />

                <div className='field'>
                  <div className='control'>
                    <button type='submit' className='button is-link is-success'>
                      Add
                    </button>
                  </div>
                </div>
              </form>
            </Card.Content>
          </Card>
  )
}

export default  graphql(getScheduleQuery)(ScheduleAdd);


// add Image
{/* <div className='field'>
<label className='label has-text-weight-normal'>
  Image
</label>
<div className='control'>
  <input 
    className='input' 
    type='file' 
    required 
    ref={fileRef}
    accept="image/png"
  />
</div>
</div> */}


// isAvailable - default is true
{/* <div className='field'>
<label className='label has-text-weight-normal'>
  Availability
</label>
<div className='control'>
  <input 
    className='input' 
    type='text' 
    required 
    value={isAvailable}
    onChange={e => 
      {
        setIsAvailable(e.target.value);
      }
    }
  />
</div>
</div> */}