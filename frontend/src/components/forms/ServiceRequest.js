import React from 'react';

const ServiceRequest = props => {
	
	let service = props.service;
	
	return(
		<form>
			<div className='field'>
				<label className='label has-text-weight-normal'>
					Service Title
				</label>
				<div className='control'>
					<input 
						className='input'
						type='text'
						value={service.title}
						disabled
					/>
				</div>
			</div>

			<div className='field'>
				<label className='label has-text-weight-normal'>
					URL
				</label>
				<div className='control'>
					<input
						className='input'
						type='text'
						value={service.url}
						disabled
					/>
				</div>
			</div>

			<div className='field'>
				<label className='label has-text-weight-normal'>
					Comments
				</label>
				<div className='control'>
					<input
						className='input'
						type='text'
						value={service.comments}
						disabled
					/>
				</div>
			</div>

			<div className='field'>
				<label className='label has-text-weight-normal'>
					Date
				</label>
				<div className='control'>
					<input
						className='input'
						type='text'
						value={service.date}
						disabled
					/>
				</div>
			</div>

			<br />

			<div className='field'>
				<div className='control'>
					<button type='submit' className='button is-link is-primary'>
						Request
					</button>
					&nbsp;
				</div>
			</div>

		</form>
	)
}

export default ServiceRequest;