import React from 'react';

const ScheduleRequest = props => {
	
	let schedule = props.schedule;
	
	return(
		<form>
			<div className='field'>
				<label className='label has-text-weight-normal'>
					Month
				</label>
				<div className='control'>
					<input 
						className='input'
						type='text'
						value={schedule.month}
						disabled
					/>
				</div>
			</div>

			<div className='field'>
				<label className='label has-text-weight-normal'>
					Date
				</label>
				<div className='control'>
					<input
						className='input'
						type='text'
						value={schedule.date}
						disabled
					/>
				</div>
			</div>

			<div className='field'>
				<label className='label has-text-weight-normal'>
					Availability
				</label>
				<div className='control'>
					<input
						className='input'
						type='text'
						value={schedule.isAvailable}
						disabled
					/>
				</div>
			</div>

			<br />

			<div className='field'>
				<div className='control'>
					<button type='submit' className='button is-link is-primary'>
						Request
					</button>
					&nbsp;
				</div>
			</div>

		</form>
	)
}

export default ScheduleRequest;