import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Section, Heading, Columns, Card } from 'react-bulma-components';
import Swal from 'sweetalert2';
import { graphql } from 'react-apollo';
import compose from 'lodash.flowright';

import { getServiceQuery } from '../../graphql/queries';
import ServiceRequest from '../forms/ServiceRequest'
import { nodeServer } from '../../functions';

const ServiceShow = (props) => {
	//console.log(props.getServiceQuery);

	console.log(props);

	const service = props.getServiceQuery.service;
	let form = <em>Loading form...</em>;
	let imageSrc = 'http://bulma.io/images/placeholders/1280x960.png';


	if(typeof service !== "undefined") {
		form = <ServiceRequest service={service}/>
	}

	if(typeof service !== "undefined" && service.imageLocation !== null ){
		// call a method that displays static files from server
		imageSrc = nodeServer() + service.imageLocation;
	}

	return(
		<Section size='medium'>
			<Heading>Service</Heading>
			<Columns>
				<Columns.Column className='is-6'>
					<Card>
						<Card.Image size="4by3" src={imageSrc}/>
						<Card.Header>
							<Card.Header.Title>Details</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							{ form }
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Section>
	);
};

export default compose(
	graphql(getServiceQuery, {
		options: props => {
			return {
				variables: {
					id: props.match.params.id
				}
			}
		},
		name: "getServiceQuery"
	})
)(ServiceShow);