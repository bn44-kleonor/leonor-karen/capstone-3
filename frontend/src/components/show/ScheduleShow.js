import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Section, Heading, Columns, Card } from 'react-bulma-components';
import Swal from 'sweetalert2';
import { graphql } from 'react-apollo';
import compose from 'lodash.flowright';

import { getScheduleQuery } from '../../graphql/queries';
import ScheduleRequest from '../forms/ScheduleRequest'
import { nodeServer } from '../../functions';


const ScheduleShow = (props) => {
	//console.log(props.getScheduleQuery);

	console.log(props);

	const schedule = props.getScheduleQuery.schedule;
	let form = <em>Loading form...</em>;
	let imageSrc = 'http://bulma.io/images/placeholders/1280x960.png';


	if(typeof schedule !== "undefined") {
		form = <ScheduleRequest schedule={schedule}/>
	}

	if(typeof schedule !== "undefined" && schedule.imageLocation !== null ){
		// call a method that displays static files from server
		imageSrc = nodeServer() + schedule.imageLocation;
	}

	return(
		<Section size='medium'>
			<Heading>Schedule</Heading>
			<Columns>
				<Columns.Column className='is-6'>
					<Card>
						<Card.Image size="4by3" src={imageSrc}/>
						<Card.Header>
							<Card.Header.Title>Details</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							{ form }
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Section>
	);
};

export default compose(
	graphql(getScheduleQuery, {
		options: props => {
			return {
				variables: {
					id: props.match.params.id
				}
			}
		},
		name: "getScheduleQuery"
	})
)(ScheduleShow);