import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Section, Heading, Columns, Card } from 'react-bulma-components';
import Swal from 'sweetalert2';
import { graphql } from 'react-apollo';
import compose from 'lodash.flowright';

import { getUserQuery } from '../../graphql/queries';
import UserRequest from '../forms/UserRequest';



const UserShow = props => {
	// console.log(props.getUserQuery);

	const user = props.getUserQuery.user;
	let form = <em>Loading form...</em>;

	if(typeof user !== "undefined") {
		form = <UserRequest user={user}/>
	}

  return (
  	<Section size='medium'>
      <Heading>User</Heading>
      <Columns>
        <Columns.Column className='is-6'>
          <Card>
            <Card.Header>
              <Card.Header.Title>Details</Card.Header.Title>
            </Card.Header>
            <Card.Content>
              { form }
            </Card.Content>
          </Card>
        </Columns.Column>
      </Columns>
    </Section>
  	);
};
 
export default compose(
	graphql(getUserQuery, {
		options: props => {
			return {
				variables: {
					id: props.match.params.id
				}
			}
		},
		name: "getUserQuery"
	})
)(UserShow)