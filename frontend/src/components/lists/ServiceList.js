import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import Swal from 'sweetalert2';

import { Card } from 'react-bulma-components';
import ServiceRow from '../rows/ServiceRow';

const ServiceList = props => {
	//console.log(props);

	let row = "";
	if(typeof props.services === "undefined" || props.services.length === 0){
		row = (
			<tr>
				<td colSpan="4">
					<em>No assets found.</em>
				</td>
			</tr>
		)
	} else {
		row = props.services.map(service => {
			return <ServiceRow 
						service={service} 
						key={service.id} 
						destroyService={props.destroyService}
						updateService={props.updateService}
					/>
		})
	}

	return (
		<Card>
			<Card.Header>
				<Card.Header.Title>
					Service List
				</Card.Header.Title>
			</Card.Header>
			<Card.Content>
				<table className='table is-fullwidth is-bordered is-striped is-hoverable'>
					<thead>
						<tr>
							<th>Title</th>
							<th>URL</th>
							<th>Comments</th>
							<th>Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{ row }
					</tbody>
				</table>
			</Card.Content>
		</Card>
	)
}

ServiceList.propTypes = {
	services: PropTypes.array
}

export default ServiceList;