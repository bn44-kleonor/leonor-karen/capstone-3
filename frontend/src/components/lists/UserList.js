import React from 'react';
import Swal from 'sweetalert2';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Card } from 'react-bulma-components';
import UserRow from '../rows/UserRow';

const UserList = props => {
  console.log(props);

  let row = "";
  if(typeof props.users === "undefined" || props.users.length === 0) {
    row = (
      <tr>
        <td colSpan="4">
          <em>No users found.</em>
        </td>
      </tr>
      )
  } else {
    row = props.users.map(user => {
      return <UserRow user={user} key={user.id} />
    })
  }

	return (
		<Card>
            <Card.Header>
              <Card.Header.Title>User List</Card.Header.Title>
            </Card.Header>
            <Card.Content>
              <table className='table is-fullwidth is-bordered is-striped is-hoverable'>
                <thead>
                  <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  { row }
                </tbody>
              </table>
            </Card.Content>
          </Card>
	)
}

export default UserList;