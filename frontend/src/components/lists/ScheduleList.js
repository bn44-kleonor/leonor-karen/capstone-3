import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import Swal from 'sweetalert2';

import { Card } from 'react-bulma-components';
import ScheduleRow from '../rows/ScheduleRow';

const ScheduleList = props => {
	//console.log(props);

	let row = "";
	if(typeof props.schedules === "undefined" || props.schedules.length === 0){
		row = (
			<tr>
				<td colSpan="4">
					<em>No schedule found.</em>
				</td>
			</tr>
		)
	} else {
		row = props.schedules.map(schedule => {
			return <ScheduleRow 
						schedule={schedule} 
						key={schedule.id} 
						destroySchedule={props.destroySchedule}
						updateSchedule={props.updateSchedule}
					/>
		})
	}

	return (
		<Card>
			<Card.Header>
				<Card.Header.Title>
					Schedule List
				</Card.Header.Title>
			</Card.Header>
			<Card.Content>
				<table className='table is-fullwidth is-bordered is-striped is-hoverable'>
					<thead>
						<tr>
							<th>Month</th>
							<th>Date</th>
							<th>Availability</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{ row }
					</tbody>
				</table>
			</Card.Content>
		</Card>
	)
}

ScheduleList.propTypes = {
	schedules: PropTypes.array
}

export default ScheduleList;