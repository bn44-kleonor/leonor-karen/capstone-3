import React, {Fragment} from 'react';
import { Link } from 'react-router-dom';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Navbar } from 'react-bulma-components';

const AppNavbar = props => {
	// console.log(props);

	return (
		<Navbar className='is-black'>
	      <Navbar.Brand>
	        <Link className="navbar-item" to="/">
	        	<strong>MERN AM</strong>
	        </Link>
	        <Navbar.Burger />
	      </Navbar.Brand>
	      <Navbar.Menu>
            <Link className="navbar-item" to="/users">User</Link>
            <Link className="navbar-item" to="/services">Service</Link>
            <Link className="navbar-item" to="/register">Register</Link>
            <Link className="navbar-item" to="/login">Login</Link>
            <Link className="navbar-item" to="/schedules">Schedules</Link>
	      </Navbar.Menu>
	    </Navbar>
	)
}

export default AppNavbar;